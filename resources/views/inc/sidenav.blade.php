
<div id="left-sidebar" class="sidebar">
    <div class="navbar-brand">
        <a href="index.html"><img src="{{url('assets/images/icon.svg')}}" alt="Oculux Logo" class="img-fluid logo"><span>Bulk Emailer</span></a>
        <button type="button" class="btn-toggle-offcanvas btn btn-sm float-right"><i class="lnr lnr-menu icon-close"></i></button>
    </div>
    <div class="sidebar-scroll">
        <div class="user-account">
            <div class="user_div">
                <img src="{{url('assets/images/user.png')}}" class="user-photo" alt="User Profile Picture">
            </div>
            <div class="dropdown">
                <span>Welcome,</span>
                <a href="javascript:void(0);" class="dropdown-toggle user-name" data-toggle="dropdown"><strong>Louis Pierce</strong></a>
                <ul class="dropdown-menu dropdown-menu-right account vivify flipInY">
                    <li><a href="{{url('/dashboard')}}"><i class="icon-user"></i>My Profile</a></li>
                    <li><a href="{{url('/dashboard')}}"><i class="icon-envelope-open"></i>Notifications</a></li>
                    <li><a href="{{url('/dashboard')}}"><i class="icon-settings"></i>Settings</a></li>
                    <li class="divider"></li>
                    <li><a href="page-login.html"><i class="icon-power"></i>Logout</a></li>
                </ul>
            </div>
        </div>
        <nav id="left-sidebar-nav" class="sidebar-nav">
            <ul id="main-menu" class="metismenu">
                <li><a href="{{url('/dashboard')}}"><i class="icon-speedometer"></i><span>Dashboard</span></a></li>
                <li><a href="{{url('/campaigns')}}"><i class="icon-envelope"></i><span>Campaigns</span></a></li>
                <li><a href="{{url('/dashboard')}}"><i class="icon-calendar"></i><span>Schedules</span></a></li>
                <li><a href="{{url('/templates')}}"><i class="icon-diamond"></i><span>Templates</span></a></li>
                <li> <a href="{{url('/mail-list')}}" ><i class="icon-book-open"></i><span>Mailing Lists</span></a></li>
                <li> <a href="{{url('/email-identity')}}" ><i class="icon-envelope"></i><span>Email Identity</span></a></li>
            </ul>
        </nav>
    </div>
</div>
