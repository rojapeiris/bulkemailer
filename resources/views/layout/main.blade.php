<!doctype html>
<html lang="en" ng-app="myApp" ng-cloak>

<head>
<title>Emailer | send bulk email</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="description" content="">
<meta name="keywords" content="">

<link rel="icon" href="favicon.ico" type="image/x-icon">
<script src="{{url('angular/angular.js')}}"></script>
<script src="{{url('angular/config.js')}}"></script>
<script>
    angular.module("myApp").constant("CSRF_TOKEN", '{{ csrf_token() }}');
    angular.module("myApp").constant("base_url", "{{ url('') }}");
</script>
<!-- VENDOR CSS -->
<link rel="stylesheet" href="{{url('assets/vendor/bootstrap/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{url('assets/vendor/font-awesome/css/font-awesome.min.css')}}">
<link rel="stylesheet" href="{{url('assets/vendor/animate-css/vivify.min.css')}}">

<link rel="stylesheet" href="{{url('assets/vendor/c3/c3.min.css')}}"/>
<link rel="stylesheet" href="{{url('assets/vendor/morrisjs/morris.css')}}" />

<!-- MAIN CSS -->
@yield('styles')
<link rel="stylesheet" href="{{url('assets/css/site.min.css')}}">
</head>

{{--  light_version --}}
<body class="font-montserrat theme-cyan">

<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="bar1"></div>
        <div class="bar2"></div>
        <div class="bar3"></div>
        <div class="bar4"></div>
        <div class="bar5"></div>
        Loading
    </div>
</div>

@auth
@include('inc.nav')
@endauth

@yield('content')

<!-- Javascript -->
<script src="{{url('assets/bundles/libscripts.bundle.js')}}"></script>
<script src={{url('assets/bundles/vendorscripts.bundle.js')}}></script>

<script src="{{url('assets/bundles/c3.bundle.js')}}"></script>
<script src="{{url('assets/bundles/knob.bundle.js')}}"></script><!-- Jquery Knob-->

<script src="{{url('assets/bundles/mainscripts.bundle.js')}}"></script>
<script src="{{url('assets/js/index6.js')}}"></script>

@yield('scripts')

</body>
</html>
