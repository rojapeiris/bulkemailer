
@extends('layout.main')

@section('content')
<div id="wrapper">

    @include('inc.sidenav')

    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12">
                        <h1>Create Campaign</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('dashboard')}}">Dashboard</a></li>
                                <li class="breadcrumb-item " aria-current="page">Campaign</li>
                                <li class="breadcrumb-item active" aria-current="page">Create</li>
                            </ol>
                        </nav>
                    </div>

                </div>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="tab-content">
                            <div class="tab-pane show active" id="e_add">
                                <div class="body">
                                    <div class="col-md-8 ">
                                        <form enctype="multipart/form-data" action="{{ route('campaign-store') }}" method="POST">
                                            @csrf
                                            <div class="mail-compose">

                                                {{-- campaign name start --}}
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">Campaign Name</span>
                                                    </div>
                                                    <input type="text" class="form-control {{($errors->first('name'))? 'parsley-error':'' }}" placeholder="Template Name" name="name">
                                                    @if($errors->first('name'))
                                                        <ul class="parsley-errors-list filled"><li class="parsley-required">{{ $errors->first('name') }}</li></ul>
                                                    @endif
                                                </div>
                                                {{-- campaign name end --}}

                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label class="input-group-text" for="inputGroupSelect01">Template</label>
                                                    </div>
                                                    <select class="custom-select {{($errors->first('template'))? 'parsley-error':'' }}" id="inputGroupSelect01" name="template">
                                                        <option selected disabled>Select Email Template</option>
                                                        @if (count($templates)>0)
                                                            @foreach ($templates as $template)
                                                                <option value="{{ $template->id }}">{{ $template->name }}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                    @if($errors->first('template'))
                                                        <ul class="parsley-errors-list filled"><li class="parsley-required">{{ $errors->first('template') }}</li></ul>
                                                    @endif
                                                </div>

                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <label class="input-group-text" for="inputGroupSelect01">Mail List</label>
                                                    </div>
                                                    <select class="custom-select {{($errors->first('maillist'))? 'parsley-error':'' }}" id="inputGroupSelect01" name="maillist">
                                                        <option selected disabled>Select Mail List</option>
                                                        @if (count($maillists)>0)
                                                            @foreach ($maillists as $maillist)
                                                                <option value="{{ $maillist->id }}">{{ $maillist->name }}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                    @if($errors->first('maillist'))
                                                        <ul class="parsley-errors-list filled"><li class="parsley-required">{{ $errors->first('maillist') }}</li></ul>
                                                    @endif
                                                </div>

                                            </div>

                                            <div class="col-12" style="margin-top:20px">
                                                <button type="submit" class="btn btn-primary">Send</button>
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">CLOSE</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@section('scripts')
    <script src="{{url('ssets/bundles/libscripts.bundle.js')}}"></script>
    <script src="{{url('assets/bundles/vendorscripts.bundle.js')}}"></script>
    <script src="{{url('assets/bundles/mainscripts.bundle.js')}}"></script>
    <script src="{{url('assets/vendor/summernote/dist/summernote.js')}}"></script>
@endsection

@section('styles')
    <link rel="stylesheet" href="{{url('assets/vendor/summernote/dist/summernote.css')}}"/>
@endsection
