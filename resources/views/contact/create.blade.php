
@extends('layout.main')

@section('content')
<div id="wrapper">

    @include('inc.sidenav')

    <div id="main-content" ng-controller="contactController">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12">
                        <h1>Contacts</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('dashboard')}}">Dashboard</a></li>
                                <li class="breadcrumb-item " aria-current="page">Mailing List</li>
                                <li class="breadcrumb-item " aria-current="page">Contacts</li>
                                <li class="breadcrumb-item active" aria-current="page">Upload</li>
                            </ol>
                        </nav>
                    </div>

                </div>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="tab-content">
                            <div class="tab-pane show active" id="e_add">
                                <div class="body">
                                    <form enctype="multipart/form-data">
                                        <div class="row clearfix">
                                            <div class="col-md-12 col-sm-12">
                                                <div class="form-group">
                                                    <input type="file" class="dropify" name="contact"  ng-file-select="onFileSelect($files)" ng-model="data.contact">
                                                    <ul class="parsley-errors-list filled invisible" id="error-contact"><li class="parsley-required" id="error-text-contact"></li></ul>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <button type="button"  ng-click="import({{$id}})" class="btn btn-primary">SAVE</button>
                                                <a  href="{{url('mail-list')}}" class="btn btn-secondary" data-dismiss="modal">CLOSE</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('styles')
<link rel="stylesheet" href="{{url('assets/vendor/sweetalert/sweetalert.css')}}">
<link rel="stylesheet" href="{{url('assets/vendor/dropify/css/dropify.min.css')}}">
@endsection

@section('scripts')
    <script src="{{url('assets/bundles/libscripts.bundle.js')}}"></script>
    <script src="{{url('assets/bundles/vendorscripts.bundle.js')}}"></script>
    <script src="{{url('assets/bundles/mainscripts.bundle.js')}}"></script>
    <script src="{{url('assets/vendor/sweetalert/sweetalert.min.js')}}"></script>
    <script src="{{url('assets/vendor/dropify/js/dropify.js')}}"></script>
    <script src="{{url('assets/js/pages/forms/dropify.js')}}"></script>
    <script src="{{url('angular/contact.js')}}"></script>
    <script>

    @if(session('success'))
        swal("Success!", "click button to close!", "success");
    @endif

    </script>
@endsection
