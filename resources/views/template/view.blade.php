
@extends('layout.main')

@section('content')
<div id="wrapper">

    @include('inc.sidenav')

    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12">
                        <h1>View Template</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('dashboard')}}">Dashboard</a></li>
                            <li class="breadcrumb-item " aria-current="page"><a href="{{url('templates')}}">Templates</a></li>
                            <li class="breadcrumb-item active" aria-current="page">View</li>
                            </ol>
                        </nav>
                    </div>

                </div>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="tab-content">
                            <div class="tab-pane show active" id="e_add">
                                <div class="body">
                                    <div class="col-md-8 ">
                                        <h4>{{ $template->name }}</h4>
                                        <p> Subject : {{ $template->subject }}</p>
                                        <p> From : {{ $template->sender }}</p>
                                        <p> Cc : {{ $template->cc }}</p>
                                        <p> Bcc : {{ $template->bcc }}</p>
                                        <p> Created at : {{ substr($template->created_at,0,10) }}</p>
                                        <hr>

                                        {!! $template->content !!}
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@section('scripts')
    <script src="{{url('ssets/bundles/libscripts.bundle.js')}}"></script>
    <script src="{{url('assets/bundles/vendorscripts.bundle.js')}}"></script>
    <script src="{{url('assets/bundles/mainscripts.bundle.js')}}"></script>
    <script src="{{url('assets/vendor/summernote/dist/summernote.js')}}"></script>
@endsection

@section('styles')
    <link rel="stylesheet" href="{{url('assets/vendor/summernote/dist/summernote.css')}}"/>
@endsection
