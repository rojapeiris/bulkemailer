
@extends('layout.main')

@section('content')
<div id="wrapper">

    @include('inc.sidenav')

    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12">
                        <h1>Create Template</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('dashboard')}}">Dashboard</a></li>
                            <li class="breadcrumb-item " aria-current="page">Templates</li>
                            <li class="breadcrumb-item active" aria-current="page">Create</li>
                            </ol>
                        </nav>
                    </div>

                </div>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="tab-content">
                            <div class="tab-pane show active" id="e_add">
                                <div class="body">
                                    <div class="col-md-8 ">
                                        <form enctype="multipart/form-data" action="{{ route('template-store') }}" method="POST">
                                            @csrf
                                            <div class="mail-compose">
                                                {{-- template name start --}}
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">Template Name</span>
                                                    </div>
                                                    <input type="text" class="form-control {{($errors->first('name'))? 'parsley-error':'' }}" placeholder="Template Name" name="name">
                                                    @if($errors->first('name'))
                                                        <ul class="parsley-errors-list filled"><li class="parsley-required">{{ $errors->first('name') }}</li></ul>
                                                    @endif
                                                </div>
                                                {{-- template name end --}}

                                                {{-- sender start --}}
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">Sender</span>
                                                    </div>
                                                    <input type="text" class="form-control {{($errors->first('sender'))? 'parsley-error':'' }}" placeholder="Sender" name="sender">
                                                    @if($errors->first('sender'))
                                                        <ul class="parsley-errors-list filled"><li class="parsley-required">{{ $errors->first('sender') }}</li></ul>
                                                    @endif
                                                </div>
                                                {{-- sender end --}}

                                                {{-- sender name start --}}
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">Sender Name</span>
                                                    </div>
                                                    <input type="text" class="form-control {{($errors->first('senderName'))? 'parsley-error':'' }}" placeholder="Sender Name" name="senderName">
                                                    @if($errors->first('senderName'))
                                                        <ul class="parsley-errors-list filled"><li class="parsley-required">{{ $errors->first('senderName') }}</li></ul>
                                                    @endif
                                                </div>
                                                {{-- sender name end --}}

                                                {{-- subject start --}}
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">Subject</span>
                                                    </div>
                                                    <input type="text" class="form-control {{($errors->first('subject'))? 'parsley-error':'' }}" placeholder="Subject" name="subject">
                                                    @if($errors->first('subject'))
                                                        <ul class="parsley-errors-list filled"><li class="parsley-required">{{ $errors->first('subject') }}</li></ul>
                                                    @endif
                                                </div>
                                                {{-- subject end --}}

                                                {{-- cc start --}}
                                                <div class="input-group mb-3">

                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">Cc</span>
                                                        </div>
                                                        <input type="text" class="form-control {{($errors->first('cc'))? 'parsley-error':'' }}" data-role="tagsinput" value="" name="cc">
                                                    </div>
                                                    @if($errors->first('cc'))
                                                        <ul class="parsley-errors-list filled"><li class="parsley-required">{{ $errors->first('cc') }}</li></ul>
                                                    @endif
                                                </div>
                                                {{-- cc end --}}

                                                 {{-- bcc start --}}
                                                 <div class="input-group mb-3">

                                                    <div class="input-group ">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">Bcc</span>
                                                        </div>
                                                        <input type="text" class="form-control  {{($errors->first('bcc'))? 'parsley-error':'' }}" data-role="tagsinput" value="" name="bcc">
                                                    </div>
                                                    @if($errors->first('bcc'))
                                                        <ul class="parsley-errors-list filled"><li class="parsley-required">{{ $errors->first('bcc') }}</li></ul>
                                                    @endif
                                                </div>
                                                {{-- bcc end --}}

                                                {{-- content start --}}
                                                <textarea class="summernote" name="content">

                                                </textarea>
                                                @if($errors->first('content'))
                                                    <ul class="parsley-errors-list filled"><li class="parsley-required">{{ $errors->first('content') }}</li></ul>
                                                @endif
                                                {{-- content end --}}
                                            </div>

                                            <div class="col-12" style="margin-top:20px">
                                                <button type="submit" class="btn btn-primary">SAVE</button>
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">CLOSE</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@section('scripts')
    <script src="{{url('ssets/bundles/libscripts.bundle.js')}}"></script>
    <script src="{{url('assets/bundles/vendorscripts.bundle.js')}}"></script>
    <script src="{{url('assets/bundles/mainscripts.bundle.js')}}"></script>
    <script src="{{url('assets/vendor/summernote/dist/summernote.js')}}"></script>
    <script src="{{url('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js')}}"></script>
@endsection

@section('styles')
    <link rel="stylesheet" href="{{url('assets/vendor/summernote/dist/summernote.css')}}"/>
    <link rel="stylesheet" href="{{url('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css')}}">
@endsection
