
@extends('layout.main')

@section('content')
<div id="wrapper">

    @include('inc.sidenav')

    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12">
                        <h1>Templates</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('dashboard')}}">Dashboard</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Templates</li>
                            </ol>
                        </nav>
                    </div>
                    <div class="col-md-6 col-sm-12 text-right hidden-xs">
                        <a href="{{url('templates/create')}}" class="btn btn-sm btn-primary" title="">Create Template</a>

                    </div>
                </div>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="table-responsive invoice_list mb-4">
                            <table class="table table-hover table-custom spacing8">
                                <thead>
                                    <tr>
                                        <th style="width: 20px;">#</th>
                                        <th>Template Name</th>
                                        <th>Sender</th>
                                        <th>Created at</th>
                                        <th class="w60">Status</th>
                                        <th class="w60">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($templates)>0)
                                        @foreach ($templates as $template )
                                            <tr>
                                                <td>
                                                    <span>01</span>
                                                </td>
                                                <td>
                                                    <div class="d-flex align-items-center">

                                                        <div class="ml-3">
                                                            {{ $template->name }}
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    {{ $template->sender }}
                                                </td>
                                                <td>
                                                    {{ substr($template->created_at,0,10) }}
                                                </td>
                                                <td>
                                                    @if ($template->status == 1)
                                                        <span class="badge badge-success ml-0 mr-0">Active</span>
                                                    @else
                                                        <span class="badge badge-danger ml-0 mr-0">Deactive</span>
                                                    @endif
                                                </td>
                                                <td>
                                                    <a href="{{ url('templates/'.$template->id.'/view') }}"  class="btn btn-sm btn-default " title="View" data-toggle="tooltip" data-placement="top"><i class="fa  fa-eye text-success"></i></a>
                                                    <a href="{{ url('templates/'.$template->id.'/edit') }}" class="btn btn-sm btn-default" title="Edit Template" data-toggle="tooltip" data-placement="top"><i class="fa fa-edit text-info"></i></a>
                                                    <button type="button" class="btn btn-sm btn-default" title="Delete" data-toggle="tooltip" data-placement="top"><i class="icon-trash text-danger"></i></button>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif

                                </tbody>
                            </table>
                            @if(count($templates)== 0)
                            <p> No Records to display</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{url('ssets/bundles/libscripts.bundle.js')}}"></script>
<script src="{{url('assets/bundles/vendorscripts.bundle.js')}}"></script>

<script src="{{url('assets/bundles/mainscripts.bundle.js')}}"></script>
@endsection
