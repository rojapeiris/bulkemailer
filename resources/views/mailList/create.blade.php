
@extends('layout.main')

@section('content')
<div id="wrapper">

    @include('inc.sidenav')

    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12">
                        <h1>Mailing Lists</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('dashboard')}}">Dashboard</a></li>
                            <li class="breadcrumb-item " aria-current="page">Mailing List</li>
                            <li class="breadcrumb-item active" aria-current="page">Create</li>
                            </ol>
                        </nav>
                    </div>

                </div>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="tab-content">
                            <div class="tab-pane show active" id="e_add">
                                <div class="body">
                                    <form action="{{route('mail-list-create')}}" method="POST">
                                        {{ csrf_field() }}
                                        <div class="row clearfix">
                                            <div class="col-md-4 col-sm-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control {{($errors->first('name'))? 'parsley-error':'' }}" placeholder="Mailing List Name" name="name" value="{{old('name')}}">
                                                    @if($errors->first('name'))
                                                        <ul class="parsley-errors-list filled"><li class="parsley-required">{{ $errors->first('name') }}</li></ul>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <button type="submit" class="btn btn-primary">SAVE</button>
                                                <a  href="{{url('mail-list')}}" class="btn btn-secondary" data-dismiss="modal">CLOSE</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('styles')
<link rel="stylesheet" href="{{url('assets/vendor/sweetalert/sweetalert.css')}}">
@endsection

@section('scripts')
    <script src="{{url('ssets/bundles/libscripts.bundle.js')}}"></script>
    <script src="{{url('assets/bundles/vendorscripts.bundle.js')}}"></script>
    <script src="{{url('assets/bundles/mainscripts.bundle.js')}}"></script>
    <script src="{{url('assets/vendor/sweetalert/sweetalert.min.js')}}"></script>

    <script>

    @if(session('success'))
        swal("Success!", "click button to close!", "success");
    @endif

    </script>
@endsection
