
@extends('layout.main')

@section('content')

<div class="pattern">
    <span class="red"></span>
</div>

<div class="auth-main2 particles_js">
    <div class="auth_div vivify fadeInTop">
        <div class="card">
            <div class="body">
                <div class="login-img">
                    <img class="img-fluid" src="{{url('assets/images/login-img.png')}}" />
                </div>
                <form class="form-auth-small" method="post" action="{{ route('login') }}">
                    {{ csrf_field() }}
                    <div class="mb-3">
                        <p class="lead">Login to your account</p>
                    </div>
                    <div class="form-group">
                        <label for="signin-email" class="control-label">Email</label>
                        <input type="email" class="form-control round {{($errors->first('email'))? 'parsley-error':'' }}" value="{{old('email')}}" name="email" >
                        @if($errors->first('email'))
                            <ul class="parsley-errors-list filled"><li class="parsley-required">{{ $errors->first('email') }}</li></ul>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="signin-password" class="control-label {{($errors->first('password'))? 'parsley-error':'' }}">Password</label>
                        <input type="password" class="form-control round" name="password">
                        @if($errors->first('password'))
                            <ul class="parsley-errors-list filled"><li class="parsley-required">{{ $errors->first('password') }}</li></ul>
                        @endif
                    </div>
                    <div class="form-group clearfix">
                        <label class="fancy-checkbox element-left">
                            <input type="checkbox">
                            <span>Remember me</span>
                        </label>
                    </div>
                    <button type="submit" class="btn btn-danger btn-round btn-block">LOGIN</button>
                    <div class="mt-4">
                        <span class="helper-text m-b-10"><i class="fa fa-lock"></i> <a href="page-forgot-password.html">Forgot password?</a></span>
                        <span>Don't have an account? <a href="{{url('register')}}">Register</a></span>
                    </div>
                </form>
                <div class="pattern">
                    <span class="red"></span>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
