
@extends('layout.main')

@section('content')

<div class="pattern">
    <span class="red"></span>
</div>

<div class="auth-main2 particles_js">
        <div class="auth_div vivify fadeInTop">
            <div class="card">
                <div class="body">
                    <div class="login-img">
                        <img class="img-fluid" src="{{url('assets/images/login-img.png')}}" />
                    </div>
                    <form class="form-auth-small" method="post" action="{{ route('register') }}">
                        {{ csrf_field() }}
                        <div class="mb-3">
                            <p class="lead">Getting Started With Bulkmailer</p>
                        </div>

                        <div class="form-group ">
                            <label for="signin-email" class="control-label ">Full Name</label>
                            <input type="text" class="form-control round {{($errors->first('name'))? 'parsley-error':'' }}" id="name"  name="name" value="{{old('name')}}">
                            @if($errors->first('name'))
                                <ul class="parsley-errors-list filled"><li class="parsley-required">{{ $errors->first('name') }}</li></ul>
                            @endif
                        </div>
                         <div class="form-group">
                            <label for="signin-email" class="control-label ">Company</label>
                            <input type="text" class="form-control round {{($errors->first('company'))? 'parsley-error':'' }}" id="company"  name="company" value="{{old('company')}}">
                            @if($errors->first('company'))
                                <ul class="parsley-errors-list filled"><li class="parsley-required">{{ $errors->first('company') }}</li></ul>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="signin-email" class="control-label ">Work Email</label>
                            <input type="email" class="form-control round {{($errors->first('email'))? 'parsley-error':'' }}" id="email" name="email" value="{{old('email')}}">
                            @if($errors->first('email'))
                                <ul class="parsley-errors-list filled"><li class="parsley-required">{{ $errors->first('email') }}</li></ul>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="signin-password" class="control-label">Password</label>
                            <input type="password" class="form-control round {{($errors->first('password'))? 'parsley-error':'' }}" id="password" name="password">
                            @if($errors->first('password'))
                                <ul class="parsley-errors-list filled"><li class="parsley-required">{{ $errors->first('password') }}</li></ul>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="signin-password" class="control-label">Password Confirmation</label>
                            <input type="password" class="form-control round" id="password"  name="password_confirmation">
                        </div>

                        <button type="submit" class="btn btn-danger btn-round btn-block">Create Account</button>
                        <div class="mt-4">
                            <span>Already have an account? <a href="{{url('/login')}}">Login</a></span>
                        </div>
                        <br>
                        <p>By creating a Bulkmailer account, you agree to the terms of the Bulkmailer Terms of Service</p>

                    </form>
                    <div class="pattern">
                        <span class="red"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
