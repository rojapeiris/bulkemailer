
@extends('layout.main')

@section('content')
<div id="wrapper">

    @include('inc.sidenav')

    <div id="main-content" ng-controller="mailListController">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12">
                        <h1>Email Identity</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('dashboard')}}">Dashboard</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Email Identity</li>
                            </ol>
                        </nav>
                    </div>
                    <div class="col-md-6 col-sm-12 text-right hidden-xs">
                        <a href="{{url('mail-list/create')}}" class="btn btn-sm btn-info" title="">Create a New Email Identity</a>

                    </div>
                </div>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="tab-content">
                            <div class="tab-pane show active" >
                                <div class="table-responsive invoice_list mb-4">
                                    <table class="table table-hover table-custom spacing8">
                                        <thead>
                                            <tr>
                                                <th style="width: 20px;">#</th>
                                                <th>Name</th>
                                                <th class="w300">Status</th>
                                                <th class="w60">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(count($emailList)>0)

                                            @php
                                                $i=1;
                                            @endphp
                                                @foreach($emailList as $mailList)
                                                    <tr id="data{{$mailList->id}}">
                                                        <td>
                                                            <span>{{$i}}</span>
                                                        </td>
                                                        <td>{{$emailList->name}}</td>
                                                        <td><span class="ml-0 mr-0"></span></td>
                                                        <td>
                                                            <a href=""  class="btn btn-sm btn-default " title="Upload" data-toggle="tooltip" data-placement="top"><i class="fa fa-upload text-success"></i></a>
                                                            <a href="" class="btn btn-sm btn-default" title="Clear Mail List" data-toggle="tooltip" data-placement="top"><i class="fa fa-eraser text-info"></i></a>
                                                            <a  type="button" class="btn btn-sm btn-default" title="Delete" data-toggle="tooltip" data-placement="top"><i class="icon-trash text-danger"></i></a>
                                                        </td>

                                                    </tr>
                                                    @php
                                                        $i++;
                                                    @endphp
                                                @endforeach
                                            @endif

                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection




@section('styles')
<link rel="stylesheet" href="{{url('assets/vendor/sweetalert/sweetalert.css')}}">
@endsection

@section('scripts')

<script src="{{url('ssets/bundles/libscripts.bundle.js')}}"></script>
<script src="{{url('assets/bundles/vendorscripts.bundle.js')}}"></script>
<script src="{{url('assets/vendor/sweetalert/sweetalert.min.js')}}"></script>
<script src="{{url('assets/bundles/mainscripts.bundle.js')}}"></script>
<script src="{{url('angular/mail-list.js')}}"></script>
@endsection
