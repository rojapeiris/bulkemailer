<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaigns', function (Blueprint $table) {
            $table->unsignedInteger('usersId');
            $table->foreign('usersId')->references('id')->on('users');
            $table->unsignedInteger('emailTemplateId');
            $table->foreign('emailTemplateId')->references('id')->on('email_templates');
            $table->bigIncrements('id');
            $table->timestamps();
            $table->unsignedInteger('total');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaigns');
    }
}
