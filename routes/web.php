<?php
use App\Mail\AWSEmailService;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//auth
Auth::routes();

//dashboard
Route::middleware('auth')->get('/dashboard', "DashboardController@index");

//mail list
Route::middleware('auth')->get('/mail-list', "MailListController@index");
Route::middleware('auth')->get('/mail-list/create', "MailListController@create");
Route::middleware('auth')->post('/mail-list/create', "MailListController@store")->name('mail-list-create');
Route::middleware('auth')->post('/mail-list/delete', "MailListController@store")->name('mail-list-delete');

//contacts
Route::middleware('auth')->get('/mail-list/{id}/contacts/create', "ContactController@create");
Route::middleware('auth')->post('/mail-list/{id}/contacts/create', "ContactController@store")->name('contact-create');
Route::middleware('auth')->get('/mail-list/{id}/contacts/destroy', "ContactController@destroy")->name('contact-destroy');


//template
Route::middleware('auth')->get('/templates', "TemplateController@index");
Route::middleware('auth')->get('/templates/create', "TemplateController@create");
Route::middleware('auth')->post('/templates/store', "TemplateController@store")->name('template-store');
Route::middleware('auth')->get('/templates/{id}/edit', "TemplateController@edit");
Route::middleware('auth')->post('/templates/{id}/update', "TemplateController@update")->name('template-update');
Route::middleware('auth')->get('/templates/{id}/view', "TemplateController@show");


//campaigns
Route::middleware('auth')->get('/campaigns', "CampaignController@index");
Route::middleware('auth')->get('/campaigns/create', "CampaignController@create");
Route::middleware('auth')->post('/campaigns/store', "CampaignController@store")->name('campaign-store');

//email identity
Route::middleware('auth')->get('/email-identity', "EmailIdentityController@index");

