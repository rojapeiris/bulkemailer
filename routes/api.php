<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//mail list
Route::get('/mail-list/{id}/delete', "MailListController@destroy");

//contact
Route::post('/mail-list/{id}/contacts/create', "ContactController@store");

Route::post('/email/send', "CampaignController@sendToOne");
