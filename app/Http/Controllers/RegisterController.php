<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function index(){
        return view('auth.register');
    }

    public function create(Request $request){

        $this->validate($request,[
            'email' => "required|email|unique|max:100",
            'password'=>'required|confirmed',
        ]);

    }
}
