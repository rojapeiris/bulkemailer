<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EmailTemplate;

use Auth;

class TemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $templates = EmailTemplate::where('status',1)->paginate(20);
        return view('template.index')->with('templates',$templates);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('template.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            "subject" => "required|string|max:191",
            "content"=> "required",
        ]);

        $template = new EmailTemplate();
        $template->usersId = Auth::User()->id;
        $template->name = $request->name;
        $template->sender = $request->sender;
        $template->senderName = $request->senderName;
        $template->subject = $request->subject;
        $template->content = $request->content;
        $template->cc = $request->cc;
        $template->bcc = $request->bcc;
        $template->status = 1;
        $template->saveOrFail();
        return back()->with('success','success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $template = EmailTemplate::findOrFail($id);
        return view('template.view')->with('template',$template);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $template = EmailTemplate::findOrFail($id);
        return view('template.edit')->with('template',$template);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $template = EmailTemplate::findOrFail($id);
        $template->name = $request->name;
        $template->sender = $request->sender;
        $template->senderName = $request->senderName;
        $template->subject = $request->subject;
        $template->content = $request->content;
        $template->cc = $request->cc;
        $template->bcc = $request->bcc;
        $template->saveOrFail();
        return back()->with('success','success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
