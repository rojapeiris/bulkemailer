<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Campaign;
use App\EmailTemplate;
use App\MailList;
use App\Http\Helpers\EmailHelper;

use Aws\Ses\SesClient;
use Aws\Exception\AwsException;

use Auth;
use Validator;

class CampaignController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::User();
        $campaigns = Campaign::where('usersId',$user->id)->orderBy('created_at','desc')->paginate(10);
        return view('campaign.index')->with('campaigns',$campaigns);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $templates = EmailTemplate::all();
        $maillists = MailList::all();
        return view('campaign.create')
                ->with('templates',$templates)
                ->with('maillists',$maillists);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request){
        $this->validate($request,[
            "template" => "required | exists:email_templates,id",
            "maillist"=>"required | exists :mail_lists,id",
            "name"=>"required",
        ]);
        set_time_limit(0);
        $maillists = MailList::findOrFail($request->maillist);
        $template = EmailTemplate::findOrFail($request->template);

        $SesClient = new SesClient([
            'region'  => 'us-east-2',
            'version'=> '2010-12-01',
            'profile'=>'default',
        ]);

        $sender_email = $template->sender;
        $configuration_set = 'ConfigSet';
        $senderName = $template->senderName;
        $subject = $template->subject;
        $html_body = $template->content;
        $char_set = 'UTF-8';

        $count = 0;

            if(count($maillists->contacts)>0){
            foreach($maillists->contacts as $contact){
                $recipient_emails = $contact->email;
                $result = $SesClient->sendEmail([
                    'Destination' => [
                        'ToAddresses' => [$recipient_emails],
                    ],
                    'ReplyToAddresses' => [$sender_email],
                    'Source' => $senderName.'<'.$sender_email.'>',
                    'Message' => [
                      'Body' => [
                          'Html' => [
                              'Charset' => $char_set,
                              'Data' => $html_body,
                          ],
                      ],
                      'Subject' => [
                          'Charset' => $char_set,
                          'Data' => $subject,
                      ],
                    ],
                ]);

                $count++;
            }
        }


        $campaign = new Campaign();
        $campaign->usersId = Auth::User()->id;
        $campaign->emailTemplateId = $request->template;
        $campaign->total = $count;
        $campaign->name = $request->name;
        $campaign->saveOrFail();
        return back()->with('success','success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function sendToOne(Request $request){

        $validation = Validator::make($request->all(),[
            "senderEmail" => "required",
            "senderName"=>"required",
            "subject"=>"required",
            "content"=>"required",
            "toAddress"=>"required"
        ]);

        if($validation->fails()){
            return json_encode(['status'=>101,"errors"=>$validation->errors()]);
        }else{
            $SesClient = new SesClient([
                'region'  => 'us-east-2',
                'version'=> '2010-12-01',
                'profile'=>'default',
            ]);

            $configuration_set = 'ConfigSet';
            $sender_email = $request->senderEmail;
            $senderName = $request->senderName;
            $subject = $request->subject;
            $html_body = $request->content;
            $toAddress = $request->toAddress;
            $char_set = 'UTF-8';

            $result = $SesClient->sendEmail([
                'Destination' => [
                    'ToAddresses' => [$toAddress],
                ],
                'ReplyToAddresses' => [$sender_email],
                'Source' => $senderName.'<'.$sender_email.'>',
                'Message' => [
                  'Body' => [
                      'Html' => [
                          'Charset' => $char_set,
                          'Data' => $html_body,
                      ],
                  ],
                  'Subject' => [
                      'Charset' => $char_set,
                      'Data' => $subject,
                  ],
                ],
            ]);

            return json_encode(["status"=>100,"data"=>$result]);
        }

    }
}
