<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EmailIdentity;
use Auth;

class EmailIdentityController extends Controller
{
    public function index(){

        $user = Auth::User();
        $emailList = EmailIdentity::where('usersId',$user->id)->get();
        return view('emailidentity.index')->with('emailList',$emailList);
    }

    public function create(){

    }
}
