<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use Validator;
use App\Contact;

use App\Http\Business\contactBusiness;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }


    public function create($id)
    {
       return view('contact.create')->with('id',$id);
    }

    public function store(Request $request,$id)
    {
        //authorization part going on here
        $responce = contactBusiness::importCSV($request,$id);
        return $responce;
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        $contacts = Contact::where('mailListId',$id)->get();
        foreach($contacts as $contact){
            $contact->delete();
        }
        return back()->with('success','success');
    }
}
