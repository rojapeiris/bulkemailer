<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Aws\Ses\SesClient;
use Aws\Exception\AwsException;

use Auth;

use App\MailList;


class MailListController extends Controller
{

    /**
     * Display a listing of the resource.
     */
    public function index()
    {

        $user = Auth::User();

        $mailList = MailList::where('usersId',$user->id)->paginate(20);
        return view('mailList.index')->with('mailList',$mailList);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('mailList.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => "required |string|max:100",
        ]);

        $user = Auth::User();

        $mailList = new MailList();
        $mailList->usersId = $user->id;
        $mailList->name = $request->name;
        $mailList->saveOrFail();

        return back()->with('success','success');
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove Mailing list with contacts
     */
    public function destroy($id)
    {
        $mailList = MailList::findOrFail($id);
        if(count($mailList->contacts)>0){
            foreach($mailList->contacts as $contact){
                $contact->delete();
            }
        }
        $mailList->delete();
        return json_encode(['status'=>'100']);
    }
}
