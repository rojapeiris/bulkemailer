<?php

namespace App\Http\Business;

use Illuminate\Http\Request;
use PHPMailer\PHPMailer\PHPMailer;
use Validator;

use App\Contact;

class contactBusiness {


    //import csv data to database
    public static function importCSV(Request $request,$id){

        $validation = Validator::make($request->all(),[
            'contact' => "required",
        ],[
            "contact.required" => "Please provide valid document."
        ]);

        if($validation->fails()){

            return json_encode(['status'=>'101','errors'=> $validation->errors()]);

        } else{

            $data = base64_decode(str_replace('base64,','',substr($request->contact, strpos($request->contact, 'base64,'))));
            $contactArray = preg_split('/\n|\r\n?/',$data);
            foreach($contactArray as $email){
                if($email != null){
                    $email = str_replace(' ','',$email);
                    $validate = contactBusiness::emailValidate($email);
                    if($validate == true){
                        $contact = new Contact();
                        $contact->mailListId = $id;
                        $contact->email = $email;
                        $contact->saveOrFail();
                    }
                }
            }
            return json_encode(['status'=>'100']);
        }
    }

    static function emailValidate($email){
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return false;
          }else{
              return true;
          }
    }
}
