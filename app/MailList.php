<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MailList extends Model
{
    public function contacts(){
        return $this->hasMany('App\Contact','mailListId','id');
    }
}
