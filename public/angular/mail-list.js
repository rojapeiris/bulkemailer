app.controller("mailListController",function($scope,$http,csrf_token,BASE_URL){

    $scope.data = {};

    $scope.delete = function(id){

        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this data!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#dc3545",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function () {

            $http(
                {
                    method : "GET",
                    url   : BASE_URL +"/api/mail-list/"+id+"/delete",
                    data :$scope.data,

                }
            ).then(
                function success(responce){

                    if(responce.data.status ==100){
                        swal("Deleted!", "Your imaginary file has been deleted.", "success");
                        $('#data'+id).remove();
                    }else if(responce.data.status ==101){

                    }
                },
                function error(responce){
                    console.log(responce);
                }
            );
        });

    };

});
