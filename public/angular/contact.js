app.controller('contactController',function($scope,$http,csrf_token,BASE_URL){

    $scope.data = {};

    $scope.import = function(id){

        $(".page-loader-wrapper").fadeIn();
        $http(
            {
                method : "POST",
                url   : BASE_URL +"/api/mail-list/"+id+"/contacts/create",
                data :$scope.data,

            }
        ).then(
            function success(responce){

                $(".page-loader-wrapper").fadeOut();

                if(responce.data.status ==100){

                    hideError('#error-contact','#error-text-contact');
                    displaySuccess();
                    $scope.data = {};

                }else if(responce.data.status ==101){

                    if(responce.data.errors.hasOwnProperty('contact')){
                        showError('#error-contact','#error-text-contact',responce.data.errors.contact);
                    }else{
                        hideError('#error-contact','#error-text-contact');
                    }
                }
            },
            function error(responce){
                console.log(responce);
            }
        );

    };


    //upload singal record
    $scope.singal = function(id){

        $(".page-loader-wrapper").fadeIn();
        $http(
            {
                method : "POST",
                url   : BASE_URL +"/api/mail-list/"+id+"/contacts/create/singal",
                data :$scope.data,

            }
        ).then(
            function success(responce){

                $(".page-loader-wrapper").fadeOut();

                if(responce.data.status ==100){

                    hideError('#error-contact','#error-text-contact');
                    displaySuccess();
                    $scope.data = {};

                }else if(responce.data.status ==101){

                    if(responce.data.errors.hasOwnProperty('contact')){
                        showError('#error-contact','#error-text-contact',responce.data.errors.contact);
                    }else{
                        hideError('#error-contact','#error-text-contact');
                    }
                }
            },
            function error(responce){
                console.log(responce);
            }
        );

    };

    //show loader

    //hide loader

});
